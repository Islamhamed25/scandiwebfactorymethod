<?php   
    
    include_once './Views/header.php'; 
    include_once './Config/inc.php';
         
    // [$errors] Global variable for show all errors to the client. 
    global $errors;
    // Database class to connect with my local database env.
    $db = new Database();

    // get the connection of database and ensure its connected successfully.
    $dbconn = $db->getConnection(); 
    
    if($_SERVER['REQUEST_METHOD'] == 'POST') {  
         
        // Product Controller that call my functionality on product model.
        $pc = new ProductController($dbconn);
        // Check the validation errors. 
        $errors = $pc->VerifyProduct(new ProductCreator() ,$_POST);
        // Get the product in a valid state.
        $product = $pc->GetProduct();

        if( !sizeof( $errors ) ) {
            if($product->Add($dbconn)) { 
              echo "<script type='text/javascript'>document.location.href='https://islamscandiwebtask.000webhostapp.com/';</script>";
                exit(); 
            } 
        }  
    }  
?> 
<div class="add-product">
    <div class="container">
        <form id="product_form" class="add-form" method="POST" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
            <input class="categoryid" type="hidden" name="categoryid">
            <div class="form-group">
                <label for="sku">SKU</label>
                <input class="form-control" id="sku"  name="sku" required value="<?php 
                       if(isset($_POST['sku'])) 
                            echo $_POST['sku'];
                 ?>"> 
                <div class="errors">
                    <?php echo $errors["sku"] ?? ' '; ?>
                </div>
            </div>
            <div class="form-group">
                <label for="name">Name</label>
                <input class="form-control" id="name" name="name" required value="<?php 
                        if(isset($_POST['name'])) 
                          echo $_POST['name'];
                ?>">
                <div class="errors">
                    <?php echo $errors["name"] ?? ' '; ?>
                </div>
            </div> 
            <div class="form-group">
                <label for="price">Price</label>
                <input  class="form-control" id="price" name="price" required value="<?php 
                       if(isset($_POST['price'])) 
                           echo $_POST['price']; 
                ?>">
                
                <div class="errors">
                    <?php echo $errors["price"] ?? ' '; ?>
                </div>
            </div>  
            <div class="form-group">
                <label for="productType">Type Switcher</label>
                <select name="type" required id="productType" onchange="getSelectedValue()" class="form-control"  style="width:300px;" >
                    <option value="none" selected disabled>Switcher</option>
                    <option value="book">Book</option>
                    <option value="dvd">DVD</option>
                    <option value="furniture">Furniture</option>
                </select>
               <div class="errors"> <p class="text-center" id="typeError" hidden>please choose the type of product!</p></div>
            </div> 
            <section class="switch-type">
                <div id="Book" hidden> 
                    <div class="form-group">
                        <label for="sizekg">Size (KG)</label>
                        <input data-categoryid=1 class="form-control" id="weight" name="weight" value="<?php 
                            if(isset($_POST['weight'])) 
                            {   
                                echo $_POST['weight'];    
                            } 
                        ?>">   
                        <div class="errors">
                            <?php echo $errors["weight"] ?? '<p style="color:#333;">please provide the weight of book in KG. </p>'; ?>
                        </div>
                    </div> 
                </div>
                <div id="DVD" class="dvd-type" hidden>
                    <div class="form-group">
                        <label for="size-mb">Size (MB)</label>
                        <input  data-categoryid=2 class="form-control" id="size" name="size" value="<?php 
                            if(isset($_POST['size'])) 
                            {   
                                echo $_POST['size']; 
                            } 
                        ?>">
                       
                        <div class="errors">
                        <?php echo $errors["size"] ?? '<p style="color:#333;">please provide the size of book in MB. </p>'; ?>
                        </div>
                     </div>
                </div>
                <div id="Furniture" class="furniture-type" hidden>
                    <div class="form-group">
                        <label for="width">Width (CM)</label>
                        <input  data-categoryid=1 class="form-control" id="width" name="width" value="<?php 
                            if(isset($_POST['width'])) 
                            {    echo $_POST['width'];  
                            }
                        ?>"> 
                        <div class="errors">
                            <?php echo $errors["width"] ?? ' '; ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="length">Length (CM) </label>
                        <input class="form-control" id="length" name="length" value="<?php 
                             if(isset($_POST['length'])) 
                                 echo $_POST['length']; 
                           
                        ?>">
                        <div class="errors">
                            <?php echo $errors["length"] ?? ' '; ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="height">Height (CM)</label>
                        <input class="form-control" id="height" name="height" value="<?php 
                              if(isset($_POST['height'])) 
                                  echo $_POST['height'];
                              
                        ?>">
                        <div class="errors">
                            <?php echo $errors["height"] ?? '<p style="color:#333;">please provide the width & height & length </p>'; ?>
                        </div>
                     </div> 
               </div>
            </section>  
            <div class="errors">
                <?php 
                    if(!( empty($errors["weight"]) && empty($errors["size"]) && empty($errors["length"]) && empty($errors["width"]) && empty($errors["height"])) )
                    echo "Your input maybe invaid check again please";
                ?>
            </div>
         </form>
    </div>
</div> 
<?php include_once './Views/footer.php';?>