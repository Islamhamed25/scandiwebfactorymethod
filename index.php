<?php

    include_once './Views/header.php'; 
    include_once './Config/inc.php';
    
    // Database class to connect with my local database env.
    $db = new Database();
    
    //Get the connection and ensure its connected successfully.
    $dbconn = $db->getConnection(); 

    //Product controller class used for call my apis.
    $pc = new ProductController($dbconn);
    $products = $pc->GetProducts();  //To Get All Products.
?>  

<hr class="sepline"> 
<div class="product-list">
    <form action="<?php echo $_SERVER["PHP_SELF"];?>" method="post" id="checked-form">
    <div class="container">
      <div class="row">  
        <?php 
         if( $products ) 
            foreach($products as $product) { 
        ?>
           <div class="product-item col-lg-3 delete-checkbox">
                <div class="checkbox pull-left">
                    <label for="<?php echo $product["Sku"];?>"></label>
                    <input name="checked_box[]" type="checkbox" id="<?php echo $product["Sku"];?>" value="<?php echo $product["Sku"];?>">
                </div>
                <div class="product-details text-center">
                    <p> <?php echo $product["Sku"]; ?></p>
                    <p><?php echo $product["Name"]; ?></p>
                    <p><?php echo $product["Price"]. " $"; ?></p>
                    <p><?php 
                            echo $size = $product["Size"] == NULL ?
                                    'Dimensions: ' 
                                    . $product["Width"]." x "
                                    . $product["Length"]." x "
                                    . $product["Height"]
                                    : 'Size: ' . $product["Size"] . (($product["CategoryId"] == 1 ) ? " Kg" : " MB" );
                    ?></p>
                </div>
          </div>
       <?php } ?> 
    </div>
  </div>
  </form> 

</div>
<?php 
    include './Views/footer.php';
?>