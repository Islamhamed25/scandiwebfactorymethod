<?php 
     
  include_once "./FactoryMethod/ProductInterface.php";
  include_once "./FactoryMethod/ProductFactory.php";
  include_once "./FactoryMethod/BookFactory.php";
  include_once "./FactoryMethod/DVDFactory.php";
  include_once "./FactoryMethod/FurnitureFactory.php";
  include_once "./FactoryMethod/BookProduct.php";
  include_once "./FactoryMethod/DVDProduct.php";
  include_once "./FactoryMethod/FurnitureProduct.php";
  include_once "./FactoryMethod/ProductController.php"; 
  include_once "./FactoryMethod/ProductCreator.php"; 
  include_once "database.php";