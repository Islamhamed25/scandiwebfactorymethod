<?php 

class DVDProduct extends Product { 
    public $size;
    public function Add($db) { 
        $this->conn = $db;
        $this->categoryid=2;
        $query = "INSERT INTO product SET Sku=:sku,Name=:name,Price=:price,Size=:size,CategoryId=:categoryid";
        $stmt = $this->conn->prepare($query); 
        $stmt->bindParam(":sku", $this->sku);
        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":price",$this->price );
        $stmt->bindParam(":size",$this->size);
        $stmt->bindParam(":categoryid",$this->categoryid); 
        
        if($stmt->execute())  return true;
        else return false;

    }
    public function SetValues($product) { 
        $this->sku = $product['sku'];
        $this->name = $product['name'];
        $this->price = $product['price'];
        $this->size = $product['size'];
        $this->categoryid = 2;
    } 
    public function Validate() { 
        $validator = new Validator($this);
        $validator->validSku();
        $validator->validName();
        $validator->validPrice();
        $validator->validSize($this->size);
        if( !sizeof($this->errors) ) { 
            return true;
        } else { 
            return false;    
        }
    } 
}