<?php 

abstract class Product { 
    public $sku;
    public $name;
    public $price; 
    public $conn;
    public $categoryid;
    public $errors = []; 
    public $dictionary;
  
    abstract public function Add($db);
    abstract public function Validate();
    abstract public function SetValues($product); 
} 