<?php    

abstract class ProductFactory{ 
    public $product; 
    public function InitalizeProducts() { 
        $products = array(
            "book" =>  ( new BookFactory() )->FactoryMethod(),
            "dvd" =>  ( new DVDFactory() )->FactoryMethod(),
            "furniture" => ( new FurnitureFactory() )->FactoryMethod()
        );  
       return $products;
    }  
    abstract public function FactoryMethod(); 
}