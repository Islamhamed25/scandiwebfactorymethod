<?php 

class FurnitureProduct extends Product { 
    public $width;
    public $height;
    public $length;
    public function Add($db) { 
        $this->conn = $db;
        $this->categoryid=3; 
        $query = "INSERT INTO product SET Sku=:sku,Name=:name,Price=:price,Width=:width,Height=:height,Length=:length,CategoryId=:categoryid";
        $stmt = $this->conn->prepare($query); 
        $stmt->bindParam(":sku", $this->sku);
        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":price",$this->price );
        $stmt->bindParam(":width",$this->width);
        $stmt->bindParam(":length",$this->length);
        $stmt->bindParam(":height",$this->height);
        $stmt->bindParam(":categoryid",$this->categoryid); 
        
        if($stmt->execute())  return true;
        else return false;
    }
    public function SetValues($product) { 
        $this->sku = $product['sku'];
        $this->name = $product['name'];
        $this->price = $product['price'];
        $this->width = $product['width'];
        $this->length = $product['length'];
        $this->height = $product['height'];
        $this->categoryid = 3;
    } 
    public function Validate() { 
        $validator = new Validator($this);
        $validator->validSku();
        $validator->validName();
        $validator->validPrice();
        $validator->validWidth($this->width);
        $validator->validLength($this->length);
        $validator->validHeight($this->height);
        if( !sizeof($this->errors) ) { 
            return true;
        } else { 
            return false;    
        }
    } 
}