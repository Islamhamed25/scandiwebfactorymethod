<?php 
  include_once "Validator.php"; 

class BookProduct extends Product { 
    public $weight; 
    public function Add($db) { 
        $this->conn = $db;
        $this->categoryid=1;
        $query = "INSERT INTO product SET Sku=:sku,Name=:name,Price=:price,Size=:weight,CategoryId=:categoryid";
        $stmt = $this->conn->prepare($query); 
        $stmt->bindParam(":sku", $this->sku);
        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":price",$this->price );
        $stmt->bindParam(":weight",$this->weight);
        $stmt->bindParam(":categoryid",$this->categoryid); 

        if($stmt->execute())  return true;
        else return false;
    }
    public function SetValues($product) { 
        $this->sku = $product['sku'];
        $this->name = $product['name'];
        $this->price = $product['price'];
        $this->weight = $product['weight'];
        $this->categoryid = 1;
    }
    public function Validate() { 
        $validator = new Validator($this);
        $validator->validSku();
        $validator->validName();
        $validator->validPrice();
        $validator->validWeight($this->weight);
        if( !sizeof($this->errors) ) { 
            return true;
        } else { 
            return false;    
        }
    }  
}