<?php 

class Validator { 
    private $product; 
    function __construct ( $product ){ 
        $this->product = $product;
    } 
    function validSku() { 
         $val = trim($this->product->sku);
         if( empty($val) ) { 
            $this->addError('sku','SKU cannot be empty.');
         } else { 
             if( strlen($val) < 6) {
                 $this->addError('sku', 'SKU must be greater than 6 chars.');
             }
         } 
    }    
    function validName() {
        $val =  htmlspecialchars( trim($this->product->name) );
        if(empty($val)) { 
            $this->addError("name","Name cannot be empty !!");
        } else { 
            if(strlen($val) < 6 ) {
                $this->addError("name","Name Must be greater than 6 chars ");
            } 
        } 
    }    
    function validPrice() {
        $price = trim($this->product->price);
        if(empty($price)) { 
            $this->addError('price','Price cannot be empty !!');
        } else { 
            if(!is_numeric($price)) 
                $this->addError('price','Price must be numerical value');   
        } 
    }
    function validWeight($weight) { 
        if(!is_numeric($weight))
            $this->addError('weight','Weight must be numerical value ');
    }
    function validSize($size) { 
        if(!is_numeric($size))
            $this->addError('size','Size must be numerical value ');
    }
    function validWidth($width) { 
        if(!is_numeric($width))
            $this->addError('width','Width must be numerical value ');
    }
    function validLength($length) { 
        if(!is_numeric($length))
            $this->addError('length','Length must be numerical value ');
    }
    function validHeight($height) { 
        if(!is_numeric($height))
            $this->addError('height','Height must be numerical value ');
    } 
    private function addError($key,$val) { 
        $this->product->errors[$key] = $val;
    } 
    
}