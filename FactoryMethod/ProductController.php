<?php 

class ProductController { 
    private $conn;
    private $product; 

    public function __construct($db) { 
        $this->conn = $db;
    }  
    public function GetProducts() { 
        $query = "select * from product Order By Sku";
        $stmt = $this->conn->prepare($query);
        if($stmt->execute() && $stmt->rowCount() > 0)
        {
          $this->products = $stmt->fetchAll(PDO::FETCH_ASSOC);
        }
        return $this->products;
    }    
    
    public function VerifyProduct($product,$post) { 
        $product = $product->getProduct($post['type']);
        $product->SetValues($post);
        $product->Validate(); 
        $errors = $product->errors; 
        $this->product = $product;
        return $errors;
    } 

    public function GetProduct() { 
        return $this->product;
    } 
    public function DeleteChecked($CheckedProducts) {  
        if(!empty($CheckedProducts) ) {
            $skus = join("','",$CheckedProducts);
            $query = "DELETE FROM product WHERE Sku IN ('$skus')";
            $stmt= $this->conn->prepare($query);
            if($stmt->execute()) { 
                return true;
            } else { 
                throw new Exception("Something unexpected happened !!");
            }
        }
    }
}
