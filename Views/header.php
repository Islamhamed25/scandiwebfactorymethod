
<?php   
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <link rel="stylesheet" href="./lib/css/Site.css"/>
    <title>Products</title> 
</head>
<body> 

<nav class="navbar navbar-default">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <?php echo ( basename( $_SERVER["PHP_SELF"]) == "index.php" ) ? "<a class='navbar-brand' href='index.php'> Product List</a>" : "<a class='navbar-brand' href='". $_SERVER["PHP_SELF"] ."'>Add Product</a>";?>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li>
          <?php echo ( basename( $_SERVER["PHP_SELF"] ) == "index.php") ? "<a href='Add.php'>ADD</a>" : "<a><button type='submit' form='product_form'> Save </button></a>";?>
        </li> 
        <li>
          <?php echo ( basename( $_SERVER["PHP_SELF"] ) == "index.php" ) ? "<a><button type='submit' form='checked-form' id='delete-product-btn'> MASS DELETE</button></a>" : "<a href='index.php'>Cancel</a>";?> 
        </li> 
      </ul>
    </div><!-- /.navbar-collapse --> 
</nav>